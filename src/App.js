import React, { useEffect, useState } from 'react'
import Header from "./components/Header";
import Product from "./components/Product"

function App() {

  const [product, setProduct] = useState([])
  const [cart, setCart] = useState([])
  const [error, setError] = useState(null);


  useEffect(() => {
    fetch('https://3sb655pz3a.execute-api.ap-southeast-2.amazonaws.com/live/product')
      .then(response => response.json())
      .then(data => setProduct({ data }))
      .catch((err) => setError(err.message))
  }, [])


  console.log(cart)



  return (
    <div>
      <Header cart={cart} />
      {error ? (
        <div className="errorContainer">
          <p>Error: {error}</p>
        </div>
      ) : (
        product.length === 0 ? (
          <div>Loading...</div>
        ) : (
          <Product data={product.data} setCart={setCart} cart={cart} />
        )
      )}
    </div>

  );
}

export default App;
