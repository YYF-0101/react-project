import React from 'react'

export const Size = ({ data, onClick, selectSize }) => {
  return (
    <>
      {
        data.map((size) => (
          <button className={`sizeSquare ${selectSize == size.label ? 'active' : ''}`} key={size.id} onClick={() => onClick(size.label)}>{size.label}</button>
        ))
      }
    </>
  )
}
