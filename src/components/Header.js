import PropTypes from 'prop-types'
import { useState } from 'react';
import Button from './Button'
import { CartItems } from './CartItems'

const Header = ({ cart }) => {

  const [showCart, setShowCart] = useState(false);
  console.log(showCart)
  console.log(cart)

  return (
    <header className='header'>
      <div className='container flex-end'>
        <Button onClick={() => { setShowCart(!showCart) }} showCart={showCart} cart={cart} />
        <div className={`dropdown-content ${showCart && 'show'}`}>
          {cart.length > 0 ? <CartItems data={cart} /> : <div>There nothing in the cart.</div>}
        </div>
      </div>
    </header>
  )
}

Header.defaultProps = {
  title: 'Task Tracker',
}

Header.propTypes = {
  title: PropTypes.string.isRequired,
}


export default Header