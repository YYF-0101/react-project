import React, { useState } from 'react'
import { Size } from './Size'

const Product = ({ data, setCart, cart }) => {
  console.log(data)
  data && console.log(data.imageURL)
  data && console.log(data.id)
  const [selectSize, setSelectSize] = useState()
  const [showAlert, setShowAlert] = useState(false);

  const onSelectSize = ((size) => {
    console.log(size)
    setSelectSize(size)
  })

  const handleAddToCart = (() => {
    if (!selectSize) {
      setShowAlert(true);

      // Hide the alert message
      setTimeout(() => {
        setShowAlert(false);
      }, 3000);
      return;
    }

    setCart([
      ...cart,
      {
        data: data,
        size: selectSize,
      },
    ]);
  })

  return (
    <div className='container'>
      {data &&
        <div className='productContainer'>
          <div className='productContainerLeft'>
            <img className='productImg' src={data.imageURL} onError={(e) => {
              e.target.alt = 'Image Failed to Load'
            }}></img>
          </div>
          <div className='productContainerRight'>
            <h3>{data.title}</h3>
            <p className='price'>${data.price.toFixed(2)}</p>
            <p className='description'>{data.description}</p>
            <p className="size">size<span>*</span><span className='selectSize'>{selectSize}</span></p>
            <div><Size data={data.sizeOptions} onClick={onSelectSize} selectSize={selectSize} /></div>
            <button className='addToCart' onClick={handleAddToCart}>
              add to cart
            </button>

            {/* render the alert message */}
            {showAlert && (
              <div className="alertMessage">
                Please select a size before adding to the cart.
                <button onClick={() => setShowAlert(false)}></button>
              </div>
            )}
          </div>
        </div>}
    </div>
  )
}

export default Product