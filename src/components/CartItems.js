import React, { useEffect, useState } from 'react'


export const CartItems = ({ data }) => {
  const [cartItem, setCartItem] = useState([]);

  useEffect(() => {
    const processedData = processCartItemData(data);
    setCartItem(processedData);
  }, [data]);

  const processCartItemData = (data) => {
    const processedData = data.reduce((acc, item) => {
      const existingItem = acc.find(
        (product) => product.size === item.size
      );

      if (existingItem) {
        existingItem.quantity += 1;
      } else {
        const newItem = { ...item, quantity: 1 };
        acc.push(newItem);
      }

      return acc;
    }, []);

    return processedData;
  };
  // console.log(data)
  // console.log(cartItem)

  return (
    <>
      {
        cartItem.map((item, index) => (
          <div className='productContainer_cart'>
            <div className='productContainerLeft'>
              <img src={item.data.imageURL} alt={item.data.title} />
            </div>
            <div className='productContainerRight'>
              <div>{item.data.title}</div>
              <div className='cartPriceStyle'>{item.quantity} x  <span className='price'>${item.data.price.toFixed(2)}</span></div>
              <div>Size: {item.size}</div>
            </div>


          </div>
        ))
      }
    </>
  )
}
