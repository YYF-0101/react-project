import React from 'react';
import PropTypes from 'prop-types'
import { FiShoppingCart } from 'react-icons/fi';


const Button = ({ onClick, showCart, cart }) => {
  const isMobile = window.innerWidth < 600;

  return (
    <button onClick={onClick} className={`btn ${showCart ? 'btnActive' : ''} `}>
      {isMobile ? (
        <>
          <FiShoppingCart />({cart.length})
        </>
      ) : (
        `My Cart(${cart.length})`
      )}
    </button>
  )
}



Button.propTypes = {
  text: PropTypes.string,
  onClick: PropTypes.func.isRequired,
  showCart: PropTypes.bool.isRequired,
}

export default Button